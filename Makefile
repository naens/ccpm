current_dir = $(shell pwd)

.PHONY: info all

FORMAT = src31n

image = src31n.img
format = src31n
size_code = 1.44M
media_type = 1_44

define cpmrun
    @echo $1
    @SDL_VIDEODRIVER=dummy SDL_AUDIODRIVER=dummy \
         dosbox -c "mount c $(current_dir)" -c "c:" -c "cpm $(1)" -c "exit" > /dev/null
endef

all: $(image)

info:
	@echo FORMAT=$(FORMAT)
	@echo image=$(image)
	@echo format=$(format)
	@echo size=$(size_code)

$(image): track0
	bximage -func=create -fd=$(size_code) -q $(image)
	mkfs.cpm -f $(format) -b track0 $(image)
	# write e5 from 0x4800 to 0x5000
	head -c 2048 /dev/zero | sed -e 's/\x00/\xe5/g' | \
		dd of=$(image) bs=1 seek=18432 count=2048 conv=notrunc
	/usr/bin/printf "\x55\xaa" | \
		dd of=$(image) bs=1 seek=510 count=2 conv=notrunc
	cpmcp -f $(format) $(image) CCPM.SYS 0:

qemu: $(image)
	qemu-system-x86_64 -drive file=$(image),format=raw,index=0,media=disk,if=floppy

bochs: $(image)
	bochs -q -f bochsrc -log /dev/null \
		'boot:a' "floppya: $(media_type)=$(image), status=inserted"

BOOT.CMD: BOOT.A86
	$(call cpmrun, asm86 boot)
	$(call cpmrun, gencmd boot 8080)

LOAD.CMD: LOAD.A86
	$(call cpmrun, asm86 load)
	$(call cpmrun, gencmd load 8080)

LBDOS.CMD: LBDOS.A86
	$(call cpmrun, asm86 lbdos)
	$(call cpmrun, gencmd lbdos 8080)

track0: BOOT.CMD LOAD.CMD LBDOS.CMD
	dd if=BOOT.CMD of=track0 bs=1 seek=0 skip=128
	dd if=LBDOS.CMD of=track0 bs=1 seek=512 skip=128
	dd if=LOAD.CMD of=track0 bs=1 seek=2816 skip=2432

clean:
	rm -f LOAD.CMD BOOT.CMD LBDOS.CMD
	rm -f *.H86 *.LST *.SYM
	rm -f *~
	rm -f track0 $(image)
	rm -f OUT
