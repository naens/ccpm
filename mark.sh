#!/usr/bin/bash

for a in "$@"
do
    k=$(echo $a | cut -f1 -d=)
    v="${a:${#k}+1}"
    if [[ -z "$v" ]]
    then
        file=$a
    else
        export "$k"=$(("$v"))
    fi

done

# we have $to but not $size

if [[ -z "$size" ]]
then
    size=$((to-from))
fi

printf 'file = %s\n' $file
printf 'from = 0x%06x\n' $from
printf 'to = 0x%06x\n' $to
printf 'size = 0x%x\n' $size
printf 'marker = 0x%0x\n' $marker

#echo -n 'OK (y/n)? '
#read answer
#if [[ "$answer" != "y" && "$answer" != "Y" ]]
#then
#    exit
#fi

m=$(printf '%02x' $marker)

if [ ! -f $1 ] || [ -z "$2" ]
then
    echo bad arguments
    exit
fi

skip=$(printf "%d" $from)
size=$(wc -c $file | cut -d ' ' -f 1)

i=$skip
while [ $i -lt $to ]
do
        n=$((i / 16))
	b1=$((n / 256))
	b2=$((n % 256))
        bb1=$(printf "%02x" $b1)
        bb2=$(printf "%02x" $b2)
        /usr/bin/printf "\x$m\x$bb1\x$bb2" | dd of=$file bs=1 seek=$i count=3 conv=notrunc status=none
        i=$(($i+16))
done

echo
