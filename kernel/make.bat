@echo off
rem // ==========================================================
rem // Makefile for Kernel and XIOS
rem // author  : Matthias Thumann
rem // revision: 22.07.04
rem // comments: !! Copy contents of D1-D4 into one directory
rem //              first !!
rem //           !! Apply patches before compiling !!
rem // ==========================================================

rem !!THESE PATHS HAVE TO BE CHANGED!!

rem // CPM emulator and CPM tools
set EMU=c:\work\emu
rem // This directory
set THISDIR=c:\work\kernel
rem // RASM86
set RASM=c:\work

echo ============================
echo Makefile for Kernel and XIOS
echo ============================
echo.

echo Copying files to kernel directory ...
ctty nul
copy %EMU%\cpm.exe %THISDIR%\cpm.exe
copy %EMU%\asm86.cmd %THISDIR%\asm86.cmd
copy %EMU%\link86.cmd %THISDIR%\link86.cmd
copy %EMU%\gencmd.cmd %THISDIR%\gencmd.cmd
copy %EMU%\genccpm.cmd %THISDIR%\genccpm.cmd
ctty con
echo ... done
echo.

echo Compiling modules ...
rem ctty nul
cpm asm86 sysdat
cpm asm86 sup
cpm asm86 rtm
cpm asm86 mem
cpm asm86 cio
%RASM%\rasm86 bdos
cpm asm86 pcxios
ctty con
echo ... done
echo.

echo Building modules ...
rem ctty nul
cpm gencmd sysdat
cpm gencmd sup 8080
cpm gencmd rtm 8080
cpm gencmd mem 8080
cpm gencmd cio 8080
cpm link86 bdos.con = bdos [data[origin[0]]]
cpm gencmd pcxios 8080
ctty nul
ren sysdat.cmd sysdat.con
ren sup.cmd sup.con
ren rtm.cmd rtm.con
ren mem.cmd mem.con
ren cio.cmd cio.con
ren pcxios.cmd xios.con
ctty con
echo ... done
echo.

echo Type
echo N, to construct system image now
echo L, to construct system image later and to keep con-files
echo.
choice /c:NL /t:L,30
if errorlevel 2 goto KEEP_CONS
cpm genccpm

echo Removing temporary files ...
ctty nul
del cpm.exe
del asm86.cmd
del link86.cmd
del gencmd.cmd
del genccpm.cmd
del sysdat.lst
del sysdat.sym
del sup.lst
del sup.sym
del rtm.lst
del rtm.sym
del mem.lst
del mem.sym
del cio.lst
del cio.sym
del bdos.lst
del bdos.sym
del bdos.obj
del pcxios.lst
del pcxios.sym
del sysdat.h86
del sup.h86
del rtm.h86
del mem.h86
del cio.h86
del pcxios.h86
del sysdat.con
del sup.con
del rtm.con
del mem.con
del cio.con
del bdos.con
del xios.con
ctty con
echo ... done
echo.

goto EOF

:KEEP_CONS
echo Removing temporary files ...
ctty nul
del asm86.cmd
del link86.cmd
del gencmd.cmd
del sysdat.lst
del sysdat.sym
del sup.lst
del sup.sym
del rtm.lst
del rtm.sym
del mem.lst
del mem.sym
del cio.lst
del cio.sym
del bdos.lst
del bdos.sym
del bdos.obj
del pcxios.lst
del pcxios.sym
del sysdat.h86
del sup.h86
del rtm.h86
del mem.h86
del cio.h86
del pcxios.h86
ctty con
echo ... done
echo.

:EOF
echo COMPILATION DONE!!