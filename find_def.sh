#/usr/bin/bash

num=0x4800
defs=$(cat /etc/cpmtools/diskdefs | grep diskdef | cut -d ' ' -f 2)
tmpdir=$(mktemp -d)

for f in $defs
do
    img=$tmpdir/image_$f.img
    mkfs.cpm -f $f -b track0 $img 2>/dev/null
    if [[ -f $img ]]
    then 
        cpmcp -f $f $img CCPM.SYS 0: 2>/dev/null
        n=$(hexdump -C $img | grep 'CCPM    SYS' | head -n 1 | cut -d ' ' -f 1)
        if [[ 0x0$n -eq $num ]]
        then
            echo $f
        fi
    fi
done
rm -rf "$tmpdir"
